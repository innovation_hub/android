package com.abnamro.minicouch.properties

/**
 * Created by tyc on 08/09/2017.
 */
object MiniCouchProperties {
    val LOCAL = true
    val SERVER_URL = if (LOCAL) "http://10.0.2.2:8080" else "http://10.0.2.2:8080"
}