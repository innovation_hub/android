package com.abnamro.minicouch

import android.annotation.SuppressLint
import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import com.abnamro.minicouch.service.authorisation.LoginService
import com.abnamro.minicouch.service.dummy.DummyModel
import com.abnamro.minicouch.service.dummy.DummyPostRestService
import com.abnamro.minicouch.service.dummy.DummyRestService
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {

    lateinit var consoleTextView: TextView
    lateinit var gButton: Button
    lateinit var pButton: Button


    @SuppressLint("SimpleDateFormat")
    private val formatter = SimpleDateFormat("HH:mm:ss:SSS")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(createLayout().view)
    }

    private fun createLayout(): AnkoContext<Context> {
        val layout = UI {
            linearLayout {
                orientation = LinearLayout.VERTICAL
                textView("Hi there")
                gButton = button("GET") {
                    onClick {
                        val drs = DummyRestService({ onResult(it) }, { onErrorMessage(it) })
                        drs.setFirstLastName("Thijs", "Eckhart")
                        executeRestService({ drs.execute() })
                    }
                }
                pButton = button("POST") {
                    onClick {
                        val drs = DummyPostRestService({ onResult(it) }, { onErrorMessage(it) })
                        drs.setFirstLastName("Thijs", "Eckhart")
                        executeRestService({ drs.execute() })
                    }
                }

                val et = editText("thijseckhart@gmail.com") {

                }

                val pw = editText("123456") {

                }

                button("Login") {
                    onClick {
                        val lrs = LoginService(onResult = { postToConsole("Login: $it") }, onError = { postToConsole("Loggin Error: $it") })
                        lrs.setLoginParamters(et.text.toString(), pw.text.toString())
                        lrs.execute()
                    }
                }
                consoleTextView = textView("Console:\n")
            }
        }
        return layout
    }

    private fun enableGUI(enable: Boolean) {
        pButton.isEnabled = enable
        gButton.isEnabled = enable
    }


    private fun executeRestService(rt: () -> AsyncTask<Unit, Unit, DummyModel?>) {
        enableGUI(false)
        rt()
    }


    private fun onResult(result: DummyModel) {
        postToConsole("$result")
        enableGUI(true)
    }

    private fun onErrorMessage(errorMessage: String) {
        postToConsole("$errorMessage")
        enableGUI(true)

    }

    private fun postToConsole(msg: String) {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = System.currentTimeMillis()
        consoleTextView.text = "${consoleTextView.text}\n ${formatter.format(calendar.time)}:$msg"
    }
}
