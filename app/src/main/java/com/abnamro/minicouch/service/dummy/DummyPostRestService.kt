package com.abnamro.minicouch.service.dummy

import com.abnamro.minicouch.service.RequestMethod
import com.abnamro.minicouch.service.RestTask
import com.abnamro.minicouch.service.buildJsonModel
import okhttp3.Response

/**
 * Created by C35534 on 24-8-2017.
 */

class DummyPostRestService(onResult: (result: DummyModel) -> Unit, onError: (errorMessage: String) -> Unit) :
        RestTask<DummyModel>(onResult, onError, "http://10.0.2.2:8080/dummypost", RequestMethod.POST) {
    override fun buildJsonModel(response: Response): DummyModel = response.buildJsonModel()

    fun setFirstLastName(first: String, last: String) {
        parameters.put("param1", first)
        parameters.put("param2", last)
    }
}
