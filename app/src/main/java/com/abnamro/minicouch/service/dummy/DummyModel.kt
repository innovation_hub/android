package com.abnamro.minicouch.service.dummy

import com.squareup.moshi.Json

/**
 * Created by Thijs Eckhart on 28-8-2017.
 */
class DummyModel(val id: Int, @Json(name = "content") val name: String = "") {
    override fun toString() = "#$id=$name"
}