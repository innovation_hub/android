package com.abnamro.minicouch.service.authorisation

import com.abnamro.minicouch.properties.MiniCouchProperties
import com.abnamro.minicouch.service.RequestMethod
import com.abnamro.minicouch.service.RestTask
import com.abnamro.minicouch.service.buildJsonModel
import com.squareup.moshi.Json
import okhttp3.Response

/**
 * Created by tyc on 08/09/2017.
 */
class LoginService(onResult: (result: LoginTokenModel) -> Unit, onError: (errorMessage: String) -> Unit) :
        RestTask<LoginTokenModel>(onResult, onError, address = "${MiniCouchProperties.SERVER_URL}/login", requestMethod = RequestMethod.POST) {
    override fun buildJsonModel(response: Response): LoginTokenModel = response.buildJsonModel()


    fun setLoginParamters(user: String, password: String) {
        parameters.put("email", user)
        parameters.put("password", password)
    }
}

class LoginTokenModel(@Json(name = "msg") val name: String = "-") {
    override fun toString() = name
}