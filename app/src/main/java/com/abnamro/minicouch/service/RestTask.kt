package com.abnamro.minicouch.service

import android.os.AsyncTask
import android.util.Log
import com.squareup.moshi.JsonDataException
import com.squareup.moshi.KotlinJsonAdapterFactory
import com.squareup.moshi.Moshi
import okhttp3.FormBody
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.util.concurrent.TimeUnit


/**
 * Created by Thijs Eckhart on 24-8-2017.
 */
abstract class RestTask<MODEL>(
        private val onResult: (result: MODEL) -> Unit,
        private val onError: (errorMessage: String) -> Unit,
        private val address: String,
        private val requestMethod: RequestMethod) : AsyncTask<Unit, Unit, MODEL?>() {
    private val TAG = RestTask::class.java.simpleName
    protected val parameters = HashMap<String, Any>()

    var errorMessage = "Unknown Error"

    override fun doInBackground(vararg p0: Unit?): MODEL? {
        val client = OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .build()
        try {
            val response = client.newCall(buildRequest()).execute()
            if (response.isSuccessful) {
                errorMessage = if (response.body() != null) {
                    try {
                        return buildJsonModel(response)
                    } catch (nullPointer: JsonDataException) {
                        Log.e(TAG, "doInBackground: Json Error", nullPointer)
                        nullPointer.message.toString()
                    } catch (e: Exception) {
                        Log.e(TAG, "Unknown Exception: ", e)
                        e.message.toString()
                    }
                } else {
                    "Empty Body"
                }
            } else {
                errorMessage = when (response.code()) {
                    200 -> "Testing 200"
                    else -> "Other Response ${response.code()}"
                }
            }
        } catch (connectException: ConnectException) {
            errorMessage = "Connection Error: $connectException"
        } catch (runTimeOut: SocketTimeoutException) {
            errorMessage = "Timeout Error: $runTimeOut"
        }
        return null
    }

    private fun buildRequest(): Request? {
        val requestBuilder = Request.Builder()
        when (requestMethod) {
            RequestMethod.POST -> {
                val formBodyBuilder = FormBody.Builder()

                for ((key, value) in parameters) {
                    formBodyBuilder.add(key, value.toString())
                }
                requestBuilder
                        .url(address)
                        .post(formBodyBuilder.build())
            }
            RequestMethod.GET -> {
                var parStr = ""
                for ((key, value) in parameters) {
                    parStr += if (parStr.isEmpty()) '?' else '&'
                    parStr += key + "=" + value
                }
                requestBuilder
                        .url(address + parStr)
            }
            else -> throw (NotImplementedError("Unknown requestMethod: $requestMethod"))
        }
        return requestBuilder.build()
    }

    /**
     * @return MODEL = response.buildJsonModel()
     * @throws JsonDataException
     */
    abstract protected fun buildJsonModel(response: Response): MODEL

    override fun onPostExecute(result: MODEL?) {
        super.onPostExecute(result)
        if (result != null) {
            onResult.invoke(result)
        } else {
            onError.invoke(errorMessage)
        }
    }
}

enum class RequestMethod {
    POST, GET
}

val kotlinMoshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()!!

/**
 *@throws JsonDataException
 */
inline fun <reified T> Response.buildJsonModel(): T {
    val issueAdapter = kotlinMoshi.adapter(T::class.java)
    return issueAdapter.fromJson(body()?.string())!!
}