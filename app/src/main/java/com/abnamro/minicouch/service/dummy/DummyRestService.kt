package com.abnamro.minicouch.service.dummy

import com.abnamro.minicouch.properties.MiniCouchProperties
import com.abnamro.minicouch.service.RequestMethod
import com.abnamro.minicouch.service.RestTask
import com.abnamro.minicouch.service.buildJsonModel
import com.squareup.moshi.Json
import okhttp3.Response

/**
 * Created by C35534 on 24-8-2017.
 */

class DummyRestService(onResult: (result: DummyModel) -> Unit, onError: (errorMessage: String) -> Unit) :
        RestTask<DummyModel>(onResult, onError, address = "${MiniCouchProperties.SERVER_URL}/dummy", requestMethod = RequestMethod.GET) {

    override fun buildJsonModel(response: Response): DummyModel = response.buildJsonModel()

    fun setFirstLastName(first: String, last: String) {
        parameters.put("param1", "Thijs")
        parameters.put("param2", "Eckhart")
    }
}

